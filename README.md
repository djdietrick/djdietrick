# Welcome!

Hello, my name is **David Dietrick** and welcome to my Gitlab page!  Feel free to take a look around and see what I've been working on.

Also be sure to check out my "[dietrick](http://gitlab.com/dietrick)" group where I keep a lot of my projects that share configurations.  

## Favorite Projects

- [Docs](http://gitlab.com/dietrick/docs) - Reference full of notes and tutorials from things that I am learning.  Built using Vitepress and hosted on Gitlab Pages.  Visit the live version at [docs.dietrick.dev](http://docs.dietrick.dev).

- [Personal Website](http://gitlab.com/dietrick/website-v3) - My personal portfolio website built using Vue.  Visit the live version at [dietrick.dev](http://dietrick.dev).

- [Phish.in Player](https://gitlab.com/dietrick/phishin-player-v2) - Media player for playing live shows by the band Phish using an API which serves audience-taped live shows.  Built using React and using AWS Lambda and S3 as the backend.  Visit the live version at [phish.dietrick.dev](https://phish.dietrick.dev).

- [Electronic Vehicle Management API](https://gitlab.com/djdietrick/electric-vehicle-management-api/-/tree/main?ref_type=heads) - API written in Python using Flask to manage a fleet of electronic vehicles as well as an algorithm to determine the optimal path to take to charge all the vehicles.

- [Music Directory Parser](https://gitlab.com/djdietrick/music-directory-parser) - Rust application that parsers local music catalogs and saves metadata to a PostgresSQL database for use by music player applications.